package br.com.rest;

public class Arte {

    private final long id;
    private final String content;
 
    public Arte(long id, String content) {
        this.id = id;
        this.content = content;
    }
 
    public long getId() {
        return id;
    }
 
    public String getContent() {
        return content;
    }
	
}
