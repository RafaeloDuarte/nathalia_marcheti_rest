package br.com.rest;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ArteController {

	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();

	@RequestMapping("/artes")
	public Arte greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
		return new Arte(counter.incrementAndGet(), String.format(template, name));
	}
}
